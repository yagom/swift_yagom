//
//  GameScene.swift
//  ShotGameSpriteKit
//
//  Created by yagom on 2014. 6. 24..
//  Copyright (c) 2014년 yagom. All rights reserved.
//

import SpriteKit

// SKSpriteNode를 상속받아 isGoingLeft라는 Bool형 인스턴스 변수를 가지는 자식클래스를 선언
// 사실 큰 필요는 없지만 예제를 위해 연습해본다.
class BirdNode: SKSpriteNode
{
    // Node가 왼쪽으로 가고 있는지를 가지고있는 Bool형 변수
    var isGoingLeft: Bool = false
}

// SKScene과 SKPhysicsContactDelegate 델리게이트 프로토콜을 따른다.
class GameScene: SKScene, SKPhysicsContactDelegate {
    // 총소리와 새소리 액션
    // waitForCompletion 값을 false로 설정하면 지난 액션이 끝나지 않았어도 중복으로 사운드 재생을 할 수 있다.
    let fireSoundAction = SKAction.playSoundFileNamed("gun_shot.caf", waitForCompletion: false)
    let birdCrowSoundAction = SKAction.playSoundFileNamed("crow.caf", waitForCompletion: false)
    
    // 참새 사이즈와 총알 사이즈
    let BirdSize = CGSize(width: 100.0, height: 100.0)
    let BulletSize = CGSize(width: 15.0, height: 15.0)
    
    // 참새와 총알의 Physics Category Mask
    let BirdPhysicsCategoryMask: UInt32 = 1 << 0
    let BulletPhysicsCategoryMask: UInt32 = 1 << 1
    
    // 이 화면으로 옮겨왔을 때 호출되는 메소드. UIKit의 viewDidAppear와 유사하다.
    override func didMoveToView(view: SKView)
    {
        self.physicsWorld.contactDelegate = self
        NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("addBird"), userInfo: nil, repeats: true)
    }
    
    // 화면을 터치했을 때 호출되는 기본 메소드이다.
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        // 총알 발사
        self.fireGun()
    }
    
    // 매 프레임이 렌더링 되기 전 실행된다. 60fps라면 1초에 60번 실행된다.
    override func update(currentTime: CFTimeInterval)
    {
        // Bird라는 이름을 가진 자식노드들을 모두 검사한다.
        self.enumerateChildNodesWithName("Bird", usingBlock:{ (bird: SKNode, shouldStop: UnsafeMutablePointer<ObjCBool>) in
            
            // 참새가 화면 밖으로 벗어난 상태라면
            if (bird.position.x < 0.0 || bird.position.x > self.frame.size.width)
            {
                // 움직이고 있지 않다면
                if (bird.hasActions() == false)
                {
                    // 화면에서 지워버린다.
                    bird.removeFromParent()
                }
            }
        } )
        
        // Bullet이라는 이름을 가진 자식노드들을 모두 검사한다.
        self.enumerateChildNodesWithName("Bullet", usingBlock:{ (bullet: SKNode, shouldStop: UnsafeMutablePointer<ObjCBool>) in
            
            // 총알이 화면 밖으로 벗어난 상태라면
            if (bullet.position.y > self.frame.size.height)
            {
                // 움직이고 있지 않다면
                if (bullet.hasActions() == false)
                {
                    // 화면에서 지워버린다.
                    bullet.removeFromParent()
                }
            }
        } )
    }
    
    // 노드를 애니메이션과 함께 특정 좌표로 이동시킨다.
    func moveToPoint(node: SKNode, point: CGPoint, duration: NSTimeInterval)
    {
        // 노드를 이동시키는 액션을 생성하고 실행한다.
        let moveAction: SKAction = SKAction.moveTo(point, duration: duration)
        node.runAction(moveAction)
    }
    
    // 임의의 좌표를 만든다.
    // 이전 예제와 동일한 코드이므로 자세한 설명은 생략한다.
    func randomPoint(screenSize: CGSize, shouldOnLeft:Bool) -> (location: CGPoint, isOnLeft: Bool)
    {
        let randIntForY = Int(rand())
        
        let randY = randIntForY % Int(screenSize.height - BirdSize.height)
        
        var randX = screenSize.width + BirdSize.width
        
        if (shouldOnLeft == true)
        {
            randX = -BirdSize.width
        }
        
        return (CGPoint(x:CGFloat(randX), y:CGFloat(randY)) , shouldOnLeft)
    }
    
    // 화면에 참새를 추가한다.
    func addBird()
    {
        // 새로운 참새 노드를 만든다.
        let bird = BirdNode(imageNamed:"bird")
        
        // 임의의 위치를 생성하여 받아온다.
        let leftOrRight: Bool = Bool(Int(rand()) % 2)
        var randomPointResult = randomPoint(self.frame.size, shouldOnLeft: leftOrRight)
        
        // 사이즈와 위치를 설정한다.
        bird.size = BirdSize
        bird.position = randomPointResult.location
        
        // 새가 어느방향으로 움직일 것인지 flag를 설정힌다. 이는 알림용으로 flag 값을 변경한다고 새가 반대로 움직이지는 않는다.
        let isGoingLeft: Bool = !randomPointResult.isOnLeft
        bird.isGoingLeft = isGoingLeft
        
        // 노드 이름을 지정한다. 이번 예제에서는 노드 이름이 didSimulatePhysics에서 사용된다.
        bird.name = "Bird"
        
        // 참새 크기만큼 물리 영역을 설정한다.
        bird.physicsBody = SKPhysicsBody(rectangleOfSize: BirdSize)
        
        // 중력의 영향을 받지 않게 한다.
        bird.physicsBody?.affectedByGravity = false
        
        // 전역으로 선언한 BirdPhysicsCategoryMask로 카테고리 마스크를 설정한다.
        bird.physicsBody?.categoryBitMask = BirdPhysicsCategoryMask
        
        // 전역으로 선언한 BulletPhysicsCategoryMask로 Collision 마스크를 설정한다.
        // Collision Bit Mask는 이 객체가 다른 객체에 맞닿을 때 물리반응을 할 객체가 가지는 마스크를 뜻한다. 즉, 이 객체는 총알 마스크를 가진 객체와 물리반응한다.
        bird.physicsBody?.collisionBitMask = BulletPhysicsCategoryMask
        
        // 전역으로 선언해준 BulletPhysicsCategoryMask로 Contact test 마스크를 설정한다.
        // 이 객체의 contact test bit mask와 부딪힌 물체의 category mask가 AND 연산했을 때 0이 아니면 didBeginContact 등 델리게이트 메서드를 호출한다.
        bird.physicsBody?.contactTestBitMask = BulletPhysicsCategoryMask
        
        // 화면에 새를 얹는다.
        self.addChild(bird)
        
        randomPointResult = randomPoint(self.frame.size, shouldOnLeft: isGoingLeft)
        
        // 새가 임의의 목표점으로 움직이게 한다.
        self.moveToPoint(bird, point: randomPointResult.location, duration: 4.0)
        
        // 새가 제대로 된 방향을 볼 수 있게 오른쪽으로 가는 새는 수평으로 뒤집는다
        if isGoingLeft == false
        {
            bird.xScale = bird.xScale * -1.0
        }
    }
    
    // 총알을 발사한다.
    func fireGun()
    {
        // 총알을 생성하고 출발점과 도착점을 생성한다.
        let bullet = SKSpriteNode(color: UIColor.blackColor(), size: BulletSize)
        let middleX = (self.frame.size.width - BulletSize.width) / 2.0
        let startPoint = CGPoint(x: middleX, y: -BulletSize.height)
        let destinationPoint = CGPoint(x: middleX, y: self.frame.size.height + BulletSize.height)
        
        // 총알의 위치, 이름, 물리영역, 카테고리 마스크 등을 설정한다.
        bullet.position = startPoint
        bullet.name = "Bullet"
        bullet.physicsBody = SKPhysicsBody(rectangleOfSize: BulletSize)
        bullet.physicsBody?.affectedByGravity = false
        bullet.physicsBody?.categoryBitMask = BulletPhysicsCategoryMask
        bullet.physicsBody?.collisionBitMask = BirdPhysicsCategoryMask
        bullet.physicsBody?.contactTestBitMask = BirdPhysicsCategoryMask
        
        // 화면에 총알을 얹어주고 목표점으로 이동하게 한다.
        self.addChild(bullet)
        self.moveToPoint(bullet, point: destinationPoint, duration: 1.5)
        
        // 총소리를 재생한다.
        bullet.runAction(self.fireSoundAction)
    }
    
    // 물리성격을 가진 노드들끼리 맞닿았을 때(충돌 했을 때) 호출되는 메서드다.
    func didBeginContact(contact: SKPhysicsContact)
    {
        var bird: BirdNode
        var bullet: SKNode
        
        if let aNode: AnyObject = contact.bodyA.node
        {
            if let bNode: AnyObject = contact.bodyB.node
            {
                // 충돌한 두 물체 중 A가 참새라면
                if (contact.bodyA.categoryBitMask == BirdPhysicsCategoryMask)
                {
                    bird = aNode as! BirdNode
                    bullet = bNode as! SKNode
                }
                    // B가 참새라면
                else if (contact.bodyB.categoryBitMask == BirdPhysicsCategoryMask)
                {
                    bullet = aNode as! SKNode
                    bird = bNode as! BirdNode
                }
                    // 이것도 저것도 아니면 무시해버리자
                else
                {
                    return
                }
                
                // 총알 애니메이션을 중지하고 화면에서 지운다.
                bullet.removeAllActions()
                bullet.removeFromParent()
                
                // 참새의 애니메이션을 중지한다.
                bird.removeAllActions()
                
                // 총에 맞은 소리를 재생한다.
                bird.runAction(self.birdCrowSoundAction)
                
                // 참새가 중력의 영향을 받도록 설정한다.
                bird.physicsBody?.affectedByGravity = true
                
                // 이후에는 아무와도 부딪히지 않도록 비트 마스크를 모두 해제한다.
                bird.physicsBody?.categoryBitMask = 0
                bird.physicsBody?.contactTestBitMask = 0
                bird.physicsBody?.collisionBitMask = 0
                
                // 참새가 왼쪽으로 가는 중이었다면
                if (bird.isGoingLeft)
                {
                    // 왼쪽으로 속도를 준다.
                    bird.physicsBody?.velocity = CGVector(dx: -250, dy: 0)
                }
                    // 오른쪽으로 가는 중이었다면
                else
                {
                    // 오른쪽으로 속도를 준다.
                    bird.physicsBody?.velocity = CGVector(dx: 250, dy: 0)
                }
            }
        }
    }
}
