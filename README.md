# README #

[만들면서 배우는 Swift : 스위프트로 시작하는 iOS 개발(http://www.hanbit.co.kr/ebook/look.html?isbn=9788968486753)] 예제 소스입니다.

예제에서 사운드 파일 등은 저작권 등의 문제로 삭제되어 있음을 알립니다.
빌드하고 실행하려면 적절한 사운드 파일을 직접 프로젝트에 추가해 주셔야 합니다.
리소스를 추가하는 방법은 [부록 - Xcode 프로젝트에 번들 리소스 추가하기] 부분을 참고해 주세요.
또, 무료사운드 파일은 http://soundbible.com/free-sound-effects-1.html, https://www.freesound.org/ 등을 통해 구하실 수 있습니다.

### 문의 및 요청사항 ###

한빛미디어 책 소개 페이지 또는 제 이메일(yagomsoft@gmail.com)로 보내주시면 감사하겠습니다 :)