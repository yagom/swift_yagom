//
//  ViewController.swift
//  PlaySound
//
//  Created by yagom on 2014. 6. 7..
//  Copyright (c) 2014년 yagom. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, AVAudioPlayerDelegate {
    
    var audioPlayer: AVAudioPlayer?             // 소리를 재생할 플레이어
    
    var progressTimer: NSTimer?                 // 플레이어의 현재 진행상황을 체크할 타이머
    
    @IBOutlet var progressSlider: UISlider?      // 플레이어의 현재 진행상황을 표시할 슬라이더
    @IBOutlet var volumeStepper: UIStepper?      // 볼륨을 조절할 스테퍼
    @IBOutlet var volumeLabel: UILabel?          // 볼륨을 표시할 레이블
    
    @IBAction func clickVoulmeStepper(stepper: UIStepper)
    {
        // 플레이어의 볼륨을 스테퍼의 값으로 설정해준다.
        self.audioPlayer?.volume = CFloat(stepper.value)
        
        // 볼륨 레이블의 텍스트를 스테퍼의 값으로 설정해준다.
        self.volumeLabel?.text = NSString(format:"%.1f", stepper.value) as String
    }
    
    @IBAction func clickPlayButton(button: UIButton)
    {
        self.audioPlayer?.play()
        makeProgeressTimer()
    }
    
    @IBAction func clickStopButton(button: UIButton)
    {
        self.audioPlayer?.stop()
        invalidateProgressTimer()
    }
    
    override func viewDidLoad()
    {
        // 번들에 위치한 사운드 파일의 주소를 가져온다.
        // 사운드 파일은 저작권 등의 문제로 프로젝트에 포함되어있지 않습니다. 적절한 음원을 프로젝트 번들파일로 넣어주세요.
        guard let bundleURL = NSBundle.mainBundle().URLForResource("sound", withExtension: "mp3") else {
            return
        }
        
        // 플레이어를 생성하고 초기화한다.
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOfURL: bundleURL)
        } catch _ {
            // 생성에 실패했을 때는 catch문으로 들어온다.
            self.audioPlayer = nil
            print("audioPlayer 초기화 실패")
        }
        
        // 플레이어의 델리게이트를 현재 ViewController 객체로 설정해준다.
        self.audioPlayer?.delegate = self
        
        // 진행 상황을 체크할 슬라이더의 초기값을 0으로 설정한다.
        self.progressSlider?.value = 0
        
        // 오버라이드했기 때문에 특별한 경우가 아니면 부모 클래스의 메소드 원형도 호출해 준다.
        super.viewDidLoad()
    }
    
    func makeProgeressTimer()
    {
        // 1초마다 checkCurrentTime 메소드를 호출하는 타이머를 생성한다.
        self.progressTimer =  NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("checkCurrentTime"), userInfo: nil, repeats: true)
    }
    
    func invalidateProgressTimer()
    {
        // 타이머를 해제한다.
        self.progressTimer?.invalidate()
    }
    
    func checkCurrentTime()
    {
        if let player = self.audioPlayer {
            // 현재 재생위치를 슬라이더에 설정해준다.
            self.progressSlider?.value = CFloat(player.currentTime / player.duration)
        }
    }
    
    // AVAudioPlayer의 Delegate 메소드 중 하나이다.
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool)
    {
        // 재생이 끝났으므로 재생위치 타이머를 해제한다.
        invalidateProgressTimer()
    }
    
    // AVAudioPlayer의 Delegate 메소드 중 하나이다.
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer, error: NSError?)
    {
        // 에러가 발생했음과 에러 설명을 볼 수 있는 얼럿창을 띄워준다.
        let alert: UIAlertController = UIAlertController(title: "에러", message: "에러가 발생했어요!", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction: UIAlertAction = UIAlertAction(title: "확인", style: UIAlertActionStyle.Cancel, handler: nil)
        alert.addAction(okAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

