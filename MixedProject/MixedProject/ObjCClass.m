//
//  ObjCClass.m
//
//  Created by yagom on 2014. 7. 20..
//  Copyright (c) 2014년 yagom. All rights reserved.
//

#import "ObjCClass.h"
#import "MixedProject-Swift.h"

@interface ObjCClass ()

@property (nonatomic, strong) NSString *myName;

@end

@implementation ObjCClass

- (instancetype)initWithMyName:(NSString *)myName
{
    self = [super init];
    if (self)
    {
        self.myName = myName;
        [self helloSwift];
    }
    return self;
}

- (NSInteger)randInteger
{
    srand((unsigned int)time(NULL));
    return rand();
}

- (NSString *)myName
{
    if (_myName)
    {
        return _myName;
    }
    
    return @"yagom";
}

- (void)helloSwift
{
    MySwiftClass *swiftClass = [[MySwiftClass alloc] init];
    NSLog(@"%@", [swiftClass helloObjC]);
}

@end
