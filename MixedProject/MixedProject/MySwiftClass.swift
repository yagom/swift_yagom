//
//  MySwiftClass.swift
//  MixedProject
//
//  Created by yagom on 2014. 7. 20..
//  Copyright (c) 2014년 yagom. All rights reserved.
//

import UIKit

class MySwiftClass: NSObject {
    func helloObjC() -> String
    {
        return "Hello Objective-C!!"
    }
}
