//
//  ViewController.swift
//  MixedProject
//
//  Created by yagom on 2014. 7. 20..
//  Copyright (c) 2014년 yagom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
                            
    override func viewDidLoad() {
        super.viewDidLoad()

        let myObjCClass = ObjCClass(myName: "your_name")
        
        print(myObjCClass.randInteger())
        
        print(myObjCClass.myName())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

