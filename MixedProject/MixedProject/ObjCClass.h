//
//  ObjCClass.h
//
//  Created by yagom on 2014. 7. 20..
//  Copyright (c) 2014년 yagom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjCClass : NSObject

- (instancetype)initWithMyName:(NSString *)myName;
- (NSInteger)randInteger;
- (NSString *)myName;

@end
