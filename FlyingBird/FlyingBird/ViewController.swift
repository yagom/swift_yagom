//
//  ViewController.swift
//  FlyingBird
//
//  Created by yagom on 2014. 6. 10..
//  Copyright (c) 2014년 yagom. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // 화면 탭 인식
    @IBOutlet var tapRecognizer: UIGestureRecognizer?
    
    // 새를 보여줄 이미지뷰
    var birdImageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // 새 이미지를 불러온다. named 메소드를 사용하면 자동으로 프로젝트 번들 내에 있는 이미지 파일을 불러온다.
        // 번들 내에 이미지파일이 없으면 nil이 할당된다. 그러므로 옵셔널 변수로 선언해준다.
        let birdImage: UIImage? = UIImage(named: "bird")
        
        // 이미지뷰 객체를 생성하고 위에서 가져온 이미지로 초기화한다.
        self.birdImageView = UIImageView(image: birdImage)
        
        // 만약 birdImage가 정상적인 UIImage 객체라면 birdSize를 가져올 수 있을 것이다.
        if let birdSize = birdImage?.size
        {
            // 이미지뷰의 프레임을 설정해준다. (0, 0) 좌표와 새이미지의 사이즈로 크기를 정했다.
            self.birdImageView?.frame = CGRect(origin: CGPointZero, size: birdSize)
        }

        // 화면에 이미지뷰를 얹어준다.
        if let birdView: UIImageView = self.birdImageView {
            self.view.addSubview(birdView)
        }
    }
    
    // UIGestureRecognizerDelegate의 한 메소드이다.
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        // 화면 안에서의 터치된 위치
        let touchPoint: CGPoint = touch.locationInView(self.view)
        
        if let birdSize: CGSize = self.birdImageView?.frame.size {
            
            // 새 이미지 사이즈와 터치된 포인트를 이용해 새가 이동할 위치를 계산한다.
            let targetPoint: CGPoint = CGPoint(x: touchPoint.x - (birdSize.width / 2) , y: touchPoint.y - (birdSize.height / 2))
            
            // 간단한 애니메이션 블록을 통해 이미지뷰의 프레임을 변경한다.
            UIView.animateWithDuration(0.5, animations: {
                self.birdImageView!.frame = CGRect(origin: targetPoint, size: birdSize)
            })
            
        }
        return true
    }
}

