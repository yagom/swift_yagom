//
//  ViewController.swift
//  ShotGame
//
//  Created by yagom on 2014. 6. 10..
//  Copyright (c) 2014년 yagom. All rights reserved.
//

import UIKit
import QuartzCore
import AVFoundation

// AnyObject를 "아무객체"라고도 표현할 수 있게 된다.
typealias 아무객체 = AnyObject

class ViewController: UIViewController {
    
    // 클래스에서 범용적으로 사용될 인스턴스 변수 선언
    var 참새이미지: UIImage?
    var 죽은새이미지: UIImage?
    var 참새배열: Array<UIImageView>?   // [UIImageView]? 와 같은 표현이다.
    var 총알배열: [UIView]?         // Array<UIView>? 와 같은 표현이다.
    var 참새사이즈: CGSize?
    var 총알맞음체크타이머: NSTimer?
    var 참새추가타이머: NSTimer?
    var 총알소리플레이어: AVAudioPlayer?
    var 새소리플레이어: AVAudioPlayer?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // 생성된 참새 객체를 넣을 배열
        self.참새배열 = [UIImageView]()
        // 생성된 총알 객체를 넣을 배열
        self.총알배열 = [UIView]()
        
        // 참새 이미지로 사용할 이미지 로드
        self.참새이미지 = UIImage(named:"bird")
        // 죽은새 이미지로 사용할 이미지 로드
        self.죽은새이미지 = UIImage(named:"birdDead")
        
        // 참새 이미지를 통한 참새 사이즈 가져오기
        self.참새사이즈 = CGSize(width: self.참새이미지!.size.width, height: self.참새이미지!.size.height)
        
        // 번들 내 총소리 파일 위치
        guard let 총소리위치 = NSBundle.mainBundle().URLForResource("gunShot", withExtension:"mp3") else {
            return
        }
        
        // 번들 내 새소리 파일 위치
        guard let 새소리위치 = NSBundle.mainBundle().URLForResource("crow", withExtension:"wav") else {
            return
        }
        
        // 총알소리를 재생할 플레이어 초기화 및 재생준비
        do {
            self.총알소리플레이어 = try AVAudioPlayer(contentsOfURL: 총소리위치) as AVAudioPlayer
            self.총알소리플레이어?.prepareToPlay()
        } catch _ {
            self.총알소리플레이어 = nil
            print("총알소리플레이어 초기화 실패")
        }
        
        // 새소리를 재생할 플레이어 초기화 및 재생준비
        do {
            self.새소리플레이어 = try AVAudioPlayer(contentsOfURL: 새소리위치) as AVAudioPlayer
            self.새소리플레이어?.prepareToPlay()
        } catch _ {
            self.새소리플레이어 = nil
            print("새소리플레이어 초기화 실패")
        }
        
        // 참새가 총알을 맞은 상태인지 0.1초 마다 체크할 타이머
        self.총알맞음체크타이머 = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("참새맞음체크:"), userInfo: nil, repeats: true)
        
        // 새로운 참새를 화면에 추가할 타이머
        self.참새추가타이머 = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("참새만들기"), userInfo: nil, repeats: true)
        
        // 총알을 발사하기 위해 Tap Gesuture Recognizer를 화면에 추가
        // delegate를 사용하지 않고도 바로 selector를 통하여 action을 설정해 줄 수도 있다.
        let 탭제스쳐: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("총알발사"))
        self.view.addGestureRecognizer(탭제스쳐)
    }
    
    // 객체가 화면에서 나타나거나 움직일 목표가 될 임의의 위치를 생성해 반환한다.
    func 임의의위치(타겟프레임: CGRect, 화면왼쪽인가:Bool) -> (위치: CGPoint, 화면왼쪽인가: Bool) {
        let y를위한임의의수 = Int(rand())
        
        // 임의의 좌표를 위한 x, y 값을 만든다.
        let 임의의Y = y를위한임의의수 % Int(타겟프레임.size.height - self.참새사이즈!.height)
        var 임의의X = 타겟프레임.size.width + self.참새사이즈!.width
        
        // 화면 왼쪽이면 새가 안보이게 하기 위하여 x좌표를 새 크기만큼 왼쪽으로 더 옮겨준다.
        if (화면왼쪽인가) {
            임의의X = -self.참새사이즈!.width
        }
        
        // 좌표값인 CGPoint와 왼쪽인지 오른쪽인지에 대한 flag 전달. 사실 flag는 꼭 필요한 것은 아니지만 예제를 위해 첨부.
        return (CGPoint(x:CGFloat(임의의X), y:CGFloat(임의의Y)) , 화면왼쪽인가)
    }
    
    // 뷰를 특정 위치까지 이동시키는 애니메이션을 만들고 실행하는 메소드
    func 뷰이동(뷰: UIView?, 위치: CGPoint, 시간: NSTimeInterval) {
        // 애니메이션 블록
        UIView.animateWithDuration(시간, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: {
            if let 이동할뷰 = 뷰 {
                이동할뷰.frame = CGRect(origin: 위치, size: 이동할뷰.frame.size)
            }
            }, completion:{
                (compeleted: Bool) in
                // 애니메이션이 끝나면
                if (compeleted == true) {
                    if let 이동된뷰 = 뷰 {
                        
                        // 배열 안의 객체 인덱스를 담을 임시변수
                        var 인덱스: Int? = nil
                        
                        // 움직인 뷰가 UIImageView의 서브클래스라면 (새라면)
                        if 이동된뷰.isKindOfClass(UIImageView) {
                            
                            // 인덱스값을 찾아서
                            인덱스 = (self.참새배열!).indexOf(이동된뷰 as! UIImageView)
                            
                            // 해당 인덱스의 참새를 지워준다.
                            self.참새배열?.removeAtIndex(인덱스!)
                            
                            // 움직임 뷰가 UIView의 서브클래스라면 (총알이라면)
                        } else if 이동된뷰.isKindOfClass(UIView) {
                            
                            // 인덱스값을 찾아서
                            인덱스 = (self.총알배열!).indexOf(이동된뷰)
                            // 해당 인덱스의 총알을 지워준다.
                            self.총알배열?.removeAtIndex(인덱스!)
                        }
                        
                        // 화면에서 우리눈에 보이지 않는다고 없어진 것이 아니다.
                        // 화면 바깥에 남아있는 뷰 객체를 화면에서 없애줘야 메모리가 해제될 것이다.
                        이동된뷰.removeFromSuperview()
                    }
                }
        })
    }
    
    // 화면에 새로운 참새를 한마리 더 추가하는 메소드
    func 참새만들기()
    {
        // 왼쪽에서 나오도록 만들지 오른쪽에서 나오도록 만들지 결정
        let 화면왼쪽인가: Bool = Bool(Int(rand()) % 2)
        
        // '임의의위치' 메소드의 결과값
        var 임의의위치결과 = self.임의의위치(self.view.bounds, 화면왼쪽인가: 화면왼쪽인가)
        
        // 참새가 출발할 위치
        var 위치 = 임의의위치결과.위치
        
        // 참새 이미지뷰 생성
        let 참새: UIImageView = UIImageView(image: self.참새이미지)
        
        // 참새이미지뷰의 프레임 조절로 나타날 위치와 사이즈 조절
        참새.frame = CGRect(origin:위치, size:self.참새사이즈!)
        
        // 화면에 참새 보여주기
        self.view.addSubview(참새)
        
        // 참새 배열에 참새 추가
        self.참새배열?.append(참새)
        
        // 참새가 이동할 임의의 위치 생성 (기존의 위치와 왼쪽 오른쪽이 반대이다)
        임의의위치결과 = self.임의의위치(self.view.bounds, 화면왼쪽인가: !화면왼쪽인가)
        위치 = 임의의위치결과.위치
        
        // 뷰이동 메소드 호출
        self.뷰이동(참새, 위치:위치, 시간:4)
    }
    
    // 화면에 새로운 총알을 쏘아올리는 메소드
    func 총알발사()
    {
        // 총알을 만들고 쏘아올린다.
        let 총알크기 = CGSize(width: 10, height: 10)
        let 총알X = (self.view.bounds.size.width - 총알크기.width) / 2.0
        let 출발위치 = CGPoint(x: 총알X, y: self.view.bounds.size.height + 총알크기.height)
        let 총알출발프레임: CGRect = CGRect(origin: 출발위치, size: 총알크기)
        let 총알도착위치 = CGPoint(x: 총알X, y: -총알크기.height)
        let 총알: UIView = UIView(frame: 총알출발프레임)
        총알.backgroundColor = UIColor.blackColor()
        
        self.view.addSubview(총알)
        self.총알배열?.append(총알)
        
        self.뷰이동(총알, 위치: 총알도착위치, 시간:1.5)
        
        // 총알을 쏘아올림과 동시에 총소리를 재생한다.
        self.총알소리플레이어?.currentTime = 0
        self.총알소리플레이어?.play()
    }
    
    // 화면에 총알에 맞은 참새가 있는지 체크한다.
    func 참새맞음체크(타이머: NSTimer)
    {
        // 참새배열안에 존재하는 참새들을 모두 검사한다.
        for 참새 : 아무객체 in self.참새배열! {
            let 참새이미지뷰: UIImageView = 참새 as! UIImageView
            
            // 참새이미지가 현재 어느 위치에 있는지 확인하기 위하여 참새 이미지뷰의 레이어를 가져온다.
            if let 참새레이어: CALayer = 참새이미지뷰.layer.presentationLayer() as? CALayer {
                
                // 총알배열에 존재하는 총알들을 모두 검사한다.
                for 총알 : 아무객체 in self.총알배열! {
                    if let 총알뷰: UIView = 총알 as? UIView {
                        if let 총알위치: CGPoint = 총알뷰.layer.presentationLayer()?.position {
                            
                            // 참새레이어와 총알레이어가 겹쳐있다면 참새가 총을 맞은 상태다.
                            if (참새레이어.hitTest(총알위치) != nil)
                            {
                                // 참새배열과 총알배열에서 참새와 총알을 꺼낸다.
                                let 참새인덱스:Int? = (self.참새배열!).indexOf(참새이미지뷰)
                                let 총알인덱스:Int? = (self.총알배열!).indexOf(총알뷰)
                                
                                // 참새인덱스를 찾았다면 (참새배열에서 참새를 찾았다면)
                                if let 인덱스 = 참새인덱스 {
                                    self.참새배열?.removeAtIndex(인덱스)
                                }
                                
                                // 총알인덱스를 찾았다면 (총알배열에서 총알를 찾았다면)
                                if let 인덱스 = 총알인덱스 {
                                    self.총알배열?.removeAtIndex(인덱스)
                                }
                                
                                // 총알과 참새 애니메이션을 중지한다.
                                총알뷰.layer.removeAllAnimations()
                                참새레이어.removeAllAnimations()
                                
                                // 참새와 총알의 현재위치를 찾는다
                                let 참새위치: CGPoint = CGPoint(x: 참새레이어.position.x - 참새이미지뷰.frame.size.width / 2, y: 참새레이어.position.y - 참새이미지뷰.frame.size.height / 2)
                                let 총알위치: CGPoint = CGPoint(x: 총알뷰.layer.position.x - 총알뷰.frame.size.width / 2, y: 총알뷰.layer.position.y - 총알뷰.frame.size.height / 2)
                                
                                // 현재 이동중인 위치로 참새 위치를 잡아준 후 죽은새 이미지로 교체한다.
                                참새이미지뷰.frame = CGRect(origin: 참새위치, size: 참새이미지뷰.frame.size)
                                참새이미지뷰.image = self.죽은새이미지
                                
                                // 총알뷰의 위치도 현재 애니메이션 중인 위치로 잡아준다.
                                총알뷰.frame = CGRect(origin: 총알위치, size: 총알뷰.frame.size)
                                
                                // 서서히 사라지는 페이드아웃 애니메이션을 위해 메소드 실행
                                self.뷰페이드아웃([총알뷰, 참새이미지뷰])
                                
                                // 새가 맞는 소리를 재생해준다.
                                self.새소리플레이어!.play()
                                break;
                            }
                        } else {
                            continue
                        }
                    } else {
                        continue
                    }
                }
            } else {
                continue
            }
        }
    }
    
    // 배열에 담긴 뷰들을 서서히 페이드아웃 시키는 애니메이션을 실행한다.
    func 뷰페이드아웃(뷰배열: [UIView])
    {
        for 객체: 아무객체 in 뷰배열
        {
            let 뷰: UIView = 객체 as! UIView
            
            // alpha 값을 이용하여 뷰를 서서히 사라지게하는 애니메이션
            UIView.animateWithDuration(0.7, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                뷰.alpha = 0
                }, completion: {
                    (value: Bool) in
                    // 애니메이션이 끝나면 뷰를 화면에서 제거한다.
                    뷰.removeFromSuperview()
            })
        }
    }
}

